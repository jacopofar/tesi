\BOOKMARK [0][-]{chapter.1}{Introduzione}{}
\BOOKMARK [0][-]{chapter.2}{La tematica Big Data}{}
\BOOKMARK [1][-]{section.2.1}{Cosa si intende per <<Big Data>>}{chapter.2}
\BOOKMARK [1][-]{section.2.2}{Contesti di utilizzo delle tecnologie Big Data}{chapter.2}
\BOOKMARK [1][-]{section.2.3}{Cloud computing}{chapter.2}
\BOOKMARK [2][-]{subsection.2.3.1}{Amazon EC2}{section.2.3}
\BOOKMARK [2][-]{subsection.2.3.2}{Heroku}{section.2.3}
\BOOKMARK [1][-]{section.2.4}{Tecnologie e modelli per il big data}{chapter.2}
\BOOKMARK [2][-]{subsection.2.4.1}{ETL e ELT}{section.2.4}
\BOOKMARK [2][-]{subsection.2.4.2}{MapReduce}{section.2.4}
\BOOKMARK [2][-]{subsection.2.4.3}{Hadoop}{section.2.4}
\BOOKMARK [3][-]{subsubsection.2.4.3.1}{Architettura di una rete Hadoop}{subsection.2.4.3}
\BOOKMARK [3][-]{subsubsection.2.4.3.2}{Tolleranza ai guasti}{subsection.2.4.3}
\BOOKMARK [3][-]{subsubsection.2.4.3.3}{Politica di scheduling}{subsection.2.4.3}
\BOOKMARK [3][-]{subsubsection.2.4.3.4}{Oozie}{subsection.2.4.3}
\BOOKMARK [2][-]{subsection.2.4.4}{Filesystem distribuiti}{section.2.4}
\BOOKMARK [2][-]{subsection.2.4.5}{ZooKeeper}{section.2.4}
\BOOKMARK [2][-]{subsection.2.4.6}{Lucene}{section.2.4}
\BOOKMARK [2][-]{subsection.2.4.7}{Solr}{section.2.4}
\BOOKMARK [2][-]{subsection.2.4.8}{Apache UIMA}{section.2.4}
\BOOKMARK [1][-]{section.2.5}{Database NoSQL}{chapter.2}
\BOOKMARK [2][-]{subsection.2.5.1}{Il CAP theorem}{section.2.5}
\BOOKMARK [2][-]{subsection.2.5.2}{Classificazione dei database NoSQL}{section.2.5}
\BOOKMARK [2][-]{subsection.2.5.3}{MongoDB}{section.2.5}
\BOOKMARK [3][-]{subsubsection.2.5.3.1}{Test di MongoDB}{subsection.2.5.3}
\BOOKMARK [3][-]{subsubsection.2.5.3.2}{I filtri di Bloom}{subsection.2.5.3}
\BOOKMARK [2][-]{subsection.2.5.4}{Linguaggi utilizzati nei sistemi Big Data}{section.2.5}
\BOOKMARK [3][-]{subsubsection.2.5.4.1}{Java}{subsection.2.5.4}
\BOOKMARK [3][-]{subsubsection.2.5.4.2}{I linguaggi funzionali per la JVM e Scala}{subsection.2.5.4}
\BOOKMARK [3][-]{subsubsection.2.5.4.3}{Hive}{subsection.2.5.4}
\BOOKMARK [3][-]{subsubsection.2.5.4.4}{Pig}{subsection.2.5.4}
\BOOKMARK [3][-]{subsubsection.2.5.4.5}{Jaql}{subsection.2.5.4}
\BOOKMARK [3][-]{subsubsection.2.5.4.6}{AQL}{subsection.2.5.4}
\BOOKMARK [1][-]{section.2.6}{IBM Biginsights}{chapter.2}
\BOOKMARK [2][-]{subsection.2.6.1}{Presentazione della piattaforma e confronto con altre soluzioni}{section.2.6}
\BOOKMARK [3][-]{subsubsection.2.6.1.1}{GPFS}{subsection.2.6.1}
\BOOKMARK [2][-]{subsection.2.6.2}{Integrazione con altre tecnologie}{section.2.6}
\BOOKMARK [2][-]{subsection.2.6.3}{Strumenti offerti}{section.2.6}
\BOOKMARK [2][-]{subsection.2.6.4}{Gestione della piattaforma}{section.2.6}
\BOOKMARK [0][-]{chapter.3}{Caso di studio}{}
\BOOKMARK [1][-]{section.3.1}{Presentazione e descrizione del problema}{chapter.3}
\BOOKMARK [1][-]{section.3.2}{Obiettivi preposti}{chapter.3}
\BOOKMARK [2][-]{subsection.3.2.1}{Il problema dell'estrazione di dati dalle pagine web}{section.3.2}
\BOOKMARK [3][-]{subsubsection.3.2.1.1}{Gestione degli script e del contenuto caricato dinamicamente}{subsection.3.2.1}
\BOOKMARK [2][-]{subsection.3.2.2}{Sentiment analysis del testo}{section.3.2}
\BOOKMARK [2][-]{subsection.3.2.3}{Abbinamento dei messaggi alla posizione geografica}{section.3.2}
\BOOKMARK [2][-]{subsection.3.2.4}{Identificazione dell'argomento}{section.3.2}
\BOOKMARK [2][-]{subsection.3.2.5}{Reporting}{section.3.2}
\BOOKMARK [0][-]{chapter.4}{Progettazione e implementazione di una soluzione}{}
\BOOKMARK [1][-]{section.4.1}{Estrazione delle informazioni da siti web generici}{chapter.4}
\BOOKMARK [2][-]{subsection.4.1.1}{Wget e httrack}{section.4.1}
\BOOKMARK [2][-]{subsection.4.1.2}{Esame di Apache Nutch}{section.4.1}
\BOOKMARK [2][-]{subsection.4.1.3}{Realizzazione di uno scraper}{section.4.1}
\BOOKMARK [3][-]{subsubsection.4.1.3.1}{Integrazione dello scraper con Mozilla Rhino}{subsection.4.1.3}
\BOOKMARK [2][-]{subsection.4.1.4}{Gestione del contesto e dei contenuti caricati dinamicamente}{section.4.1}
\BOOKMARK [2][-]{subsection.4.1.5}{Esecuzione parallela dello scraper}{section.4.1}
\BOOKMARK [2][-]{subsection.4.1.6}{Analisi della scalabilit\340 dello scraper}{section.4.1}
\BOOKMARK [3][-]{subsubsection.4.1.6.1}{Progettazione di un benchmark}{subsection.4.1.6}
\BOOKMARK [3][-]{subsubsection.4.1.6.2}{Implementazione del benchmark}{subsection.4.1.6}
\BOOKMARK [3][-]{subsubsection.4.1.6.3}{Risultati ottenuti e loro significato}{subsection.4.1.6}
\BOOKMARK [2][-]{subsection.4.1.7}{Implementazione di un filtro di Bloom}{section.4.1}
\BOOKMARK [0][-]{chapter.5}{Analisi dei risultati}{}
\BOOKMARK [0][-]{chapter.6}{Conclusioni}{}
\BOOKMARK [0][-]{chapter*.24}{Bibliografia}{}
\BOOKMARK [1][-]{section.6.1}{WordCount.java}{chapter*.24}
\BOOKMARK [1][-]{section.6.2}{WordCount con Pig Latin}{chapter*.24}
\BOOKMARK [1][-]{section.6.3}{WordCount in Jaql}{chapter*.24}
\BOOKMARK [0][-]{chapter*.26}{Elenco delle tabelle}{}
