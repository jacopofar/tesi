\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{9}{chapter.1}
\contentsline {chapter}{\numberline {2}La tematica Big Data}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Cosa si intende per <<Big Data>>}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Contesti di utilizzo delle tecnologie Big Data}{13}{section.2.2}
\contentsline {section}{\numberline {2.3}Cloud computing}{13}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Amazon EC2}{13}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Heroku}{13}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Tecnologie e modelli per il big data}{13}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}ETL e ELT}{13}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}MapReduce}{16}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Hadoop}{18}{subsection.2.4.3}
\contentsline {subsubsection}{\numberline {2.4.3.1}Architettura di una rete Hadoop}{20}{subsubsection.2.4.3.1}
\contentsline {subsubsection}{\numberline {2.4.3.2}Tolleranza ai guasti}{20}{subsubsection.2.4.3.2}
\contentsline {subsubsection}{\numberline {2.4.3.3}Politica di scheduling}{22}{subsubsection.2.4.3.3}
\contentsline {subsubsection}{\numberline {2.4.3.4}Oozie}{22}{subsubsection.2.4.3.4}
\contentsline {subsection}{\numberline {2.4.4}Filesystem distribuiti}{23}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}ZooKeeper}{25}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Lucene}{26}{subsection.2.4.6}
\contentsline {subsection}{\numberline {2.4.7}Solr}{26}{subsection.2.4.7}
\contentsline {subsection}{\numberline {2.4.8}Apache UIMA}{27}{subsection.2.4.8}
\contentsline {section}{\numberline {2.5}Database NoSQL}{27}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Il CAP theorem}{28}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Classificazione dei database NoSQL}{29}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}MongoDB}{32}{subsection.2.5.3}
\contentsline {subsubsection}{\numberline {2.5.3.1}Test di MongoDB}{35}{subsubsection.2.5.3.1}
\contentsline {paragraph}{Installazione e configurazione}{36}{section*.7}
\contentsline {paragraph}{Importazione del file XML}{36}{section*.8}
\contentsline {paragraph}{Versioning basato sui timestamp}{40}{section*.10}
\contentsline {subsubsection}{\numberline {2.5.3.2}I filtri di Bloom}{41}{subsubsection.2.5.3.2}
\contentsline {subsection}{\numberline {2.5.4}Linguaggi utilizzati nei sistemi Big Data}{43}{subsection.2.5.4}
\contentsline {subsubsection}{\numberline {2.5.4.1}Java}{43}{subsubsection.2.5.4.1}
\contentsline {subsubsection}{\numberline {2.5.4.2}I linguaggi funzionali per la JVM e Scala}{47}{subsubsection.2.5.4.2}
\contentsline {subsubsection}{\numberline {2.5.4.3}Hive}{51}{subsubsection.2.5.4.3}
\contentsline {subsubsection}{\numberline {2.5.4.4}Pig}{51}{subsubsection.2.5.4.4}
\contentsline {paragraph}{Gestione del parallelismo con UDF arbitrarie}{52}{section*.12}
\contentsline {subsubsection}{\numberline {2.5.4.5}Jaql}{53}{subsubsection.2.5.4.5}
\contentsline {subsubsection}{\numberline {2.5.4.6}AQL}{54}{subsubsection.2.5.4.6}
\contentsline {section}{\numberline {2.6}IBM Biginsights}{54}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Presentazione della piattaforma e confronto con altre soluzioni}{55}{subsection.2.6.1}
\contentsline {subsubsection}{\numberline {2.6.1.1}GPFS}{56}{subsubsection.2.6.1.1}
\contentsline {subsection}{\numberline {2.6.2}Integrazione con altre tecnologie}{59}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Strumenti offerti}{60}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}Gestione della piattaforma}{60}{subsection.2.6.4}
\contentsline {chapter}{\numberline {3}Caso di studio}{61}{chapter.3}
\contentsline {section}{\numberline {3.1}Presentazione e descrizione del problema}{61}{section.3.1}
\contentsline {section}{\numberline {3.2}Obiettivi preposti}{62}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Il problema dell'estrazione di dati dalle pagine web}{62}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}Gestione degli script e del contenuto caricato dinamicamente}{63}{subsubsection.3.2.1.1}
\contentsline {subsection}{\numberline {3.2.2}Sentiment analysis del testo}{64}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Abbinamento dei messaggi alla posizione geografica}{64}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Identificazione dell'argomento}{65}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Reporting}{65}{subsection.3.2.5}
\contentsline {chapter}{\numberline {4}Progettazione e implementazione di una soluzione}{67}{chapter.4}
\contentsline {section}{\numberline {4.1}Estrazione delle informazioni da siti web generici}{67}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Wget e httrack}{67}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Esame di Apache Nutch}{68}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Realizzazione di uno scraper}{70}{subsection.4.1.3}
\contentsline {paragraph}{Scelta di un sistema di scripting per aumentare la flessibilit\`a}{73}{section*.20}
\contentsline {subsubsection}{\numberline {4.1.3.1}Integrazione dello scraper con Mozilla Rhino}{73}{subsubsection.4.1.3.1}
\contentsline {paragraph}{Compilazione a runtime del codice JavaScript e esecuzione concorrente}{74}{section*.21}
\contentsline {subsection}{\numberline {4.1.4}Gestione del contesto e dei contenuti caricati dinamicamente}{76}{subsection.4.1.4}
\contentsline {paragraph}{Estrazione agile dei contenuti caricati dinamicamente}{77}{section*.22}
\contentsline {subsection}{\numberline {4.1.5}Esecuzione parallela dello scraper}{79}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Analisi della scalabilit\`a dello scraper}{79}{subsection.4.1.6}
\contentsline {subsubsection}{\numberline {4.1.6.1}Progettazione di un benchmark}{80}{subsubsection.4.1.6.1}
\contentsline {subsubsection}{\numberline {4.1.6.2}Implementazione del benchmark}{81}{subsubsection.4.1.6.2}
\contentsline {subsubsection}{\numberline {4.1.6.3}Risultati ottenuti e loro significato}{82}{subsubsection.4.1.6.3}
\contentsline {subsection}{\numberline {4.1.7}Implementazione di un filtro di Bloom}{82}{subsection.4.1.7}
\contentsline {chapter}{\numberline {5}Analisi dei risultati}{85}{chapter.5}
\contentsline {chapter}{\numberline {6}Conclusioni}{87}{chapter.6}
\contentsline {chapter}{Bibliografia}{91}{chapter*.24}
\contentsline {section}{\numberline {6.1}WordCount.java}{93}{section.6.1}
\contentsline {section}{\numberline {6.2}WordCount con Pig Latin}{95}{section.6.2}
\contentsline {section}{\numberline {6.3}WordCount in Jaql}{95}{section.6.3}
\contentsline {chapter}{Elenco delle tabelle}{97}{chapter*.26}
