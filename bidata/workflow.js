function workflow(executor){
	for(var i=1;i<400;i=i+1){
		executor.notifica("K-folding cycle #"+i);
		//partition data
		executor.setConfig(JSON.stringify(
		{"filter_type":"hash","seed":i,"max":"100",
            "threshold":"20","criterion":"greater"}));
		executor.runPhase("filter","original.json","training_"+i+".json");
		executor.setConfig(JSON.stringify(
		{"filter_type":"hash","seed":i,"max":"100",
            "threshold":"20","criterion":"lower"}));
		executor.runPhase("fiter","original.json","validation_"+i+".json");
		//token count
        executor.setConfig(JSON.stringify({
            "tag_suffix":"sentiment","word_distance":2,
            "stem":false}));
        executor.runPhase("tag count","training_"+i+".json","counted_"+i+".json");
        executor.setConfig(JSON.stringify({
            "factor":1,"tag_suffix":"sentiment","tags":["positive","negative","neutral"]}));
        executor.runPhase("smoothing","counted_"+i+".json","smoothed_"+i+".json");
		executor.runPhase("polarity","smoothed_"+i+".json","model_"+i+".json");
        //classification
		executor.setConfig(JSON.stringify({
            "model_file":"model_"+i+".json",
            "tag_suffix":"sentiment",
            "tags":["positive","negative","neutral"]}));
		executor.runPhase("classificate","validation_"+i+".json","assigned_"+i+".json");
        //save correct/wrong counters
		executor.runPhase("count equals","assigned_"+i+".json","precision_"+i+".json");
	}
}
