package importazione;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;


/**
 * Legge un dump di en.wiktionary già presente in GridFS e lo salva al suo interno sotto forma di documenti
 * È un benchmark fatto per testare le prestazioni con diverse dimensioni dell'inserimento in tronconi 
 * */
public class Lettura {
	//la dimensione del segmento da committare
	static int dimensione=100;
	static int esaminati=0;
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DB db = new Mongo("10.28.95.74").getDB("test");
		GridFS myFS = new GridFS(db);  
		GridFSDBFile dump = myFS.find("decompresso.xml").iterator().next();
		InputStream is = dump.getInputStream();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser      saxParser = factory.newSAXParser();

		DefaultHandler handler   = new SaxHandler(db);
		System.out.println("Avvio parsing con blocchi per l'inserimento di dimensione "+dimensione);
		long inizio = System.currentTimeMillis();
		saxParser.parse(is, handler);
		System.out.println("Importazione completata con blocchi da "+dimensione+", ci ho messo "+(System.currentTimeMillis()-inizio)/1000+" secondi, ossia "+(System.currentTimeMillis()-inizio)/esaminati+" ms per documento");
	}

}

class SaxHandler extends DefaultHandler {
	boolean inTitle=false,inText=false;
	String titolo="";
	String contenuto="";
	private DB db;
	private DBCollection coll;
	
	BasicDBObject[] pendenti=new BasicDBObject[Lettura.dimensione];
	public SaxHandler(DB db){
		this.db=db;
		this.coll = db.getCollection("wiktionaryArticles");
	}
	public void startDocument() throws SAXException {
		System.out.println("Avvio parser");
	}

	public void endDocument() throws SAXException {
		//inserisco gli elementi mancanti
		coll.insert(pendenti);
		System.out.println("parsing completato");
	}

	public void startElement(String uri, String localName,
			String qName, Attributes attributes)
					throws SAXException {
		if(qName.equals("title")) inTitle=true;
		if(qName.equals("text")) inText=true;
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if(qName.equals("title")) inTitle=false;
		if(qName.equals("text")) {
			System.out.println("estratto l'articolo #"+Lettura.esaminati+": "+titolo);


			//salvo l'articolo
			BasicDBObject doc = new BasicDBObject();
			doc.put("titolo", titolo);
			doc.put("content", contenuto);
			pendenti[Lettura.esaminati%Lettura.dimensione]=doc;
			Lettura.esaminati++;
			//effettuo il commit
			if(Lettura.esaminati%Lettura.dimensione==0)coll.insert(pendenti);
			titolo="";
			contenuto="";
			inText=false;
		}
	}

	public void characters(char ch[], int start, int length)
			throws SAXException {
		if (inTitle) titolo+=String.copyValueOf(ch, start, length);
		if (inText) contenuto+=String.copyValueOf(ch, start, length);
	}

	public void ignorableWhitespace(char ch[], int start, int length)
			throws SAXException {
	}

}    
