import cherrypy
from random import shuffle
from random import randint
from random import choice
from time import time
class CollatzPage:
	intestazione='''
	<html>
	<head><title> titolo d'esempio</title>
	</head>
	<body>
	'''
	seriali=list("abcdefg hijklmnopq rstuvzxyws")
	def index(self):
		with open("/home/biadmin/Desktop/eclipse_copiato/stresstest_scraper/accessi.log", "a") as myfile:
			myfile.write("\n"+str(time())+"\tINDEX")
		return '''
		<a href="./A/632">prova questo</a><br/>
		<a href="./B/48">e questo</a><br/>
		<a href="./C/9000">E anche questo</a><br/>
		'''
	index.exposed = True
	def default(self,serial,number):
		with open("/home/biadmin/Desktop/eclipse_copiato/stresstest_scraper/accessi.txt", "a") as myfile:
			myfile.write("\n"+str(time())+"\t"+serial+"\t"+number)
		number=int(number)
		r=self.intestazione
		if (number%2==0):
			r+='<br /><a href="../'+serial+'/'+str(number/2)+'">dimezza</a>'
		else:
			r+='<br /><a href="../'+serial+'/'+str(number*3+1)+'">triplica pi&ugrave; uno</a>'
		
		r+='<br /><a href="../'+choice(self.seriali)+'/'+str(randint(1,10000000))+'">NUOVO!</a>'
		shuffle(self.seriali)
		r+='<br /><strong>'+(''.join(self.seriali))+'</strong></body></html>'
		return r
	default.exposed = True


import os.path
tutconf = os.path.join(os.path.dirname(__file__), 'tutorial.conf')

if __name__ == '__main__':
    # CherryPy always starts with app.root when trying to map request URIs
    # to objects, so we need to mount a request handler root. A request
    # to '/' will be mapped to HelloWorld().index().
    cherrypy.quickstart(CollatzPage(), config=tutconf)
else:
    # This branch is for the test suite; you can ignore it.
    cherrypy.tree.mount(UsersPage(), config=tutconf)
